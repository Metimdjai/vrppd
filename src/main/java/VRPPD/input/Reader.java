package VRPPD.input;


import VRPPD.insertion.Customer;
import VRPPD.insertion.CustomerRequest;
import edu.uci.ics.jung.graph.util.Pair;

import java.io.*;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

    private LinkedList<CustomerRequest> customers; //list of customer requests during the start of the problem
    private int capacity;//capacity of vehicles, 0=infinite capacity

    //constructor

    public Reader(String path){
        customers=new LinkedList<CustomerRequest>();
        int i;
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        assert fstream!=null;

        //use file
        DataInputStream in = new DataInputStream(fstream);
        //read input
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        try {
            String line2 = br.readLine();
            this.capacity=Integer.parseInt(line2);//first line contains capacity, if 0 vehicles can serve unlimited customers
            if(this.capacity==0) {
                this.capacity = -1;
            }
            line2=br.readLine();
            int dim=Integer.parseInt(line2);//second line containts number of requests at the beginning (lines of the file)
            for(i=0;i<dim;i++){
                line2=br.readLine();

                String line=line2.replaceAll("\\s+"," ");

                Pattern p=Pattern.compile("([^ ]+) (\\d+) (\\d+) ([-+]?[0-9]*\\.?[0-9]*) ([-+]?[0-9]*\\.?[0-9]*) (\\d+) (\\d+) (\\d+) ([-+]?[0-9]*\\.?[0-9]*) ([-+]?[0-9]*\\.?[0-9]*) (\\d+) (\\d+)");
                Matcher m=p.matcher(line);
                if(!m.find()){
                    System.out.println("Pattern matching failed");
                }

                String name;
                Double addedToProblem,serviceTime1,serviceTime2;
                Pair<Double> location1,timeWindow1,location2,timeWindow2;

                name=m.group(1);
                addedToProblem=Double.parseDouble(m.group(2));
                serviceTime1=Double.parseDouble(m.group(3));
                location1=new Pair<Double>(Double.parseDouble(m.group(4)),Double.parseDouble(m.group(5)));
                timeWindow1=new Pair<Double> (Double.parseDouble(m.group(6)),Double.parseDouble(m.group(7)));
                serviceTime2=Double.parseDouble(m.group(8));
                location2=new Pair<Double>(Double.parseDouble(m.group(9)),Double.parseDouble(m.group(10)));
                timeWindow2=new Pair<Double>(Double.parseDouble(m.group(11)),Double.parseDouble(m.group(12)));

                //customers are stored in +,- pairs to accomodate the problem

                Customer customerPlus=new Customer(name,addedToProblem,serviceTime1,location1,timeWindow1,true);
                Customer customerMinus=new Customer(name,addedToProblem,serviceTime2,location2,timeWindow2,false);

                customers.add(new CustomerRequest(customerPlus,customerMinus));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //accessors

    public LinkedList<CustomerRequest> getCustomers(){
        return customers;
    }

    public int getCapacity(){
        return capacity;
    }

    //output

    @Override

    public String toString(){
        return customers.toString();
    }
}
