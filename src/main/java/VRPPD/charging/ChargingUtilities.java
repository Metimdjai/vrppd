package VRPPD.charging;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import VRPPD.insertion.Route;
import VRPPD.insertion.Utilities;
import org.apache.log4j.Logger;

import edu.uci.ics.jung.graph.util.Pair;
import VRPPD.insertion.CustomerRequest;

public class ChargingUtilities {

	static Logger LOGGER = Logger.getLogger(ChargingUtilities.class);

	static boolean prints=false;

	public static List<Pair<Double>> getChargingStations(LinkedList<CustomerRequest> customers){
		Double xMin = 0.0, xMax = 0.0, yMin = 0.0, yMax = 0.0;

		for(CustomerRequest customer : customers){
			Pair<Double> start = customer.getRequest().getFirst().getLocation();
			Pair<Double> end = customer.getRequest().getSecond().getLocation();
			if(xMin>start.getFirst())
				xMin = start.getFirst();
			if(xMin>end.getFirst())
				xMin = end.getFirst();
			if(xMax<start.getFirst())
				xMax = start.getFirst();
			if(xMax<end.getFirst())
				xMax = end.getFirst();
			if(yMin>start.getSecond())
				yMin = start.getSecond();
			if(yMin>end.getSecond())
				yMin = end.getSecond();
			if(yMax<start.getSecond())
				yMax = start.getSecond();
			if(yMax<end.getSecond())
				yMax = end.getSecond();
		}

		if(prints) {
			LOGGER.info("Xmin: " + xMin + " , yMin: " + yMin + " , xMax: " + xMax + " , yMax: " + yMax);
		}

		List<Pair<Double>> chargingStations = new ArrayList<Pair<Double>>(7);
		Random generator = new Random(23);

		chargingStations.add(new Pair<Double>(20.0,30.0));
		for(int i=1;i<7;i+=2){
			chargingStations.add(new Pair<Double>(xMin + ((xMax+xMin)/2.0-xMin)*generator.nextDouble(), (yMax+yMin)/2.0 + (yMax-(yMax+yMin)/2.0)*generator.nextDouble()));
		}

		for(int i=2;i<7;i+=2){
			chargingStations.add(new Pair<Double>((xMax+xMin)/2.0 + (xMax-(xMax+xMin)/2.0)*generator.nextDouble(), yMin + ((yMax+yMin)/2.0-yMin)*generator.nextDouble()));
		}

        if(prints) {
            for (int j = 0; j < 7; j++) {
                System.out.println(chargingStations.get(j));
            }
        }

		return chargingStations;
	}

    public static List<Pair<Double>> getChargingStations2(LinkedList<CustomerRequest> customers){
        Double xMin = 0.0, xMax = 0.0, yMin = 0.0, yMax = 0.0;

        for(CustomerRequest customer : customers){
            Pair<Double> start = customer.getRequest().getFirst().getLocation();
            Pair<Double> end = customer.getRequest().getSecond().getLocation();
            if(xMin>start.getFirst())
                xMin = start.getFirst();
            if(xMin>end.getFirst())
                xMin = end.getFirst();
            if(xMax<start.getFirst())
                xMax = start.getFirst();
            if(xMax<end.getFirst())
                xMax = end.getFirst();
            if(yMin>start.getSecond())
                yMin = start.getSecond();
            if(yMin>end.getSecond())
                yMin = end.getSecond();
            if(yMax<start.getSecond())
                yMax = start.getSecond();
            if(yMax<end.getSecond())
                yMax = end.getSecond();
        }

        if(prints) {
            LOGGER.info("Xmin: " + xMin + " , yMin: " + yMin + " , xMax: " + xMax + " , yMax: " + yMax);
        }

        List<Pair<Double>> chargingStations = new ArrayList<Pair<Double>>(customers.size()/5);
        Random generator = new Random(23);

        chargingStations.add(new Pair<Double>(20.0,30.0));
        for(int i=0;i<20-1;i++){
            chargingStations.add(new Pair<Double>(xMin + generator.nextInt(xMax.intValue()) + generator.nextDouble(), yMin + generator.nextInt(yMax.intValue()) + generator.nextDouble()));
        }

        return chargingStations;
    }

	//original battery capacity

    public static Double getBatteryCapacity(LinkedList<Route> fleet,List<Pair<Double>> chargingStations){

        Double averageLength=Utilities.getAverageRouteLength(fleet);//get average route length for current problem
        Double furthestChargingStation=Utilities.getFurthestDistance(fleet,chargingStations);//get max distance between a customer and a charging station

        //battery capacity=max(0.6*averageRouteLength,2*furthestChargingStation)

        return Math.max(0.6*averageLength, 2.0*furthestChargingStation);

    }

    //new battery capacity

	public static Double getBatteryCapacity2(LinkedList<Route> fleet,List<Pair<Double>> chargingStations){
        boolean entered=false;
        Double max=0.0;
        for(Route r:fleet){
            if(!entered){
                entered=true;
                max=r.getRouteLength();
            }
            else if(r.getRouteLength()>max){
                max=r.getRouteLength();
            }
        }
        return 0.6*max;

    }


	public static int getSoonerFeasibleRecharge(Route route,Double expectedWastedTime){
		int pos=-1;

        for(int i=1;i<route.getCustomers().size();i++){
            double timeBetween = (route.getMargins().get(i).getFirst() - route.getMargins().get(i-1).getFirst());
            double timeNeeded = Utilities.getDistance(route.getCustomers().get(i).getLocation(), route.getCustomers().get(i-1).getLocation());
            if(Math.abs(timeNeeded-timeBetween)>=expectedWastedTime){
				if(prints) {
					LOGGER.info("wasted time " + Math.abs(timeNeeded - timeBetween));
				}
                pos=i;
                return pos;
            }
        }

		return pos;
	}

}
