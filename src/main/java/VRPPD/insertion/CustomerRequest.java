package VRPPD.insertion;


import edu.uci.ics.jung.graph.util.Pair;

public class CustomerRequest implements Comparable{
    private Pair<Customer> request;

    public CustomerRequest(Customer customerPlus,Customer customerMinus){
        request=new Pair<Customer>(customerPlus,customerMinus);
    }

    public Pair<Customer> getRequest(){
        return request;
    }

    //comparison method

    public int compareTo(Object request2) throws ClassCastException {
        if(!(request2 instanceof CustomerRequest)) {
            throw new ClassCastException("CustomerRequest object expected.");
        }
        Customer c1,c2,c3,c4;
        c1=request.getFirst();
        c2=request.getSecond();
        c3=((CustomerRequest) request2).request.getFirst();
        c4=((CustomerRequest) request2).request.getSecond();

        Double slack1,slack2;

        if(c1==null){
            slack1=c2.getTimeWindow().getSecond();
        }
        else if(c2==null){
            slack1=c1.getTimeWindow().getSecond();
        }
        else{
            slack1=c2.getTimeWindow().getSecond()-c1.getTimeWindow().getFirst()-Utilities.getDistance(c1.getLocation(),c2.getLocation());
        }

        if(c3==null){
            slack2=c4.getTimeWindow().getSecond();
        }
        else if(c4==null){
            slack2=c3.getTimeWindow().getSecond();
        }
        else{
            slack2=c4.getTimeWindow().getSecond()-c3.getTimeWindow().getFirst()-Utilities.getDistance(c3.getLocation(),c4.getLocation());
        }

        return (int) (slack1-slack2);
    }

    //output

    @Override

    public String toString(){
        return (request.getFirst().getName());
    }
}
