package VRPPD.insertion;


import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import VRPPD.checks.ConstraintsChecker;
import VRPPD.insertion.optimizer.InsertionEvaluator;
import VRPPD.onlineAlgo.OnlineVRPPD.Mode;
import edu.uci.ics.jung.graph.util.Pair;

public class Utilities {

	static Logger LOGGER = Logger.getLogger(Utilities.class);

    static boolean prints=false;

    //simple Euclidean distance between two points

	public static double getDistance(Pair<Double> p1,Pair<Double> p2){
		return Math.sqrt(Math.pow((p1.getFirst()-p2.getFirst()), 2) + Math.pow((p1.getSecond()-p2.getSecond()), 2));
	}
	
	 //method to find best Insertion of pickup and delivery request of a client

    public static Route findBestInsertion(Customer customerPlus,Customer customerMinus,Route route,List<Pair<Double>> chargingStations,Mode mode,Double batteryCapacity,Double rechargeTime,Double rechargeRate){
        InsertionEvaluator evaluator=new InsertionEvaluator();
        Route bestInsertion;

        //first customer insertion, do it without any checks (we assume there is enough battery to service a pickup-delivery pair and get to the closest recharging station at least)

		if(route.getCustomers().isEmpty()){
        	//LOGGER.info("Inserting in an empty route");
            bestInsertion=new Route(route);
            if(customerPlus!=null)
                bestInsertion.addCustomer(customerPlus);
            if(customerMinus!=null)
                bestInsertion.addCustomer(customerMinus);

            return bestInsertion;
        }


        for(int i=route.getIndex()+1;i<=route.getCustomers().size();i++){//for every position in the list
            Route tmpRoute=new Route(route);

            if(customerPlus!=null)//if customer+ exists (a client might only require pickup without delivery)
                tmpRoute.addCustomerTo(customerPlus,i);//try inserting customer

            if(ConstraintsChecker.isFeasible(tmpRoute,Mode.NO_RECHARGE,batteryCapacity)){//if the insertion is feasible (no battery checking yet, so mode is 0)
                for(int j=i+1;j<=tmpRoute.getCustomers().size();j++) {//for every position AFTER customer+ insertion
                    Route tmpRoute2 = new Route(tmpRoute);
                    if (customerMinus != null){//if customer- exists (a client might only require delivery without pickup)
                        tmpRoute2.addCustomerTo(customerMinus, j);//try inserting customer-
                    }

                    if (mode!=Mode.NO_RECHARGE&&ConstraintsChecker.isFeasible(tmpRoute2,Mode.NO_RECHARGE,batteryCapacity)) {//if algorithm is running with a recharging strategy

                        assert chargingStations!=null;//ensure recharging stations exist

                        //add a dummy unfixed station at the end of the list
                        tmpRoute2.addCustomer(new Customer("R - 0.0",0.0,0.0,new Pair<Double>(0.0,0.0),new Pair<Double>(0.0,0.0),true));

                        //set it to how it was supposed to be if it will need to be fixed
                        tmpRoute2.setRechargeStation(batteryCapacity,rechargeTime,mode,rechargeRate,tmpRoute2.getCustomers().size()-1,false);

                        int oldSegment=tmpRoute2.getSegment();

                        tmpRoute2.setSegment(tmpRoute2.getCustomers().size()-1);

                        //if adding c+,c- (or one of them if the other does not exist) still ensures a closest recharging station by the end of the current route segment is reachable

                        if(ConstraintsChecker.isFeasible(tmpRoute2,mode,batteryCapacity)) {
                            //customer's pickup and delivery requests were successfully scheduled and the vehicle can reach it's corresponding charging station if necessary, add solution to feasibles

                            //first remove the dummy recharging station and set segment back to where it was (last station does not need to be fixed yet)
                            tmpRoute2.removeLastCustomer();
                            tmpRoute2.setSegment(oldSegment);
                            evaluator.addFeasibleOptimization(tmpRoute2);

                        }
                        else{//adding both c+,c- is not feasible, therefore try...


                            //a) setting a recharge station by the end of the route and adding c- after it...
                            if(customerPlus!=null) {

                                //start from list which has c+ set
                                tmpRoute2 = new Route(tmpRoute);

                                //add a dummy unfixed station at the end of the list
                                tmpRoute2.addCustomer(new Customer("R - 0.0", 0.0, 0.0, new Pair<Double>(0.0, 0.0), new Pair<Double>(0.0, 0.0), true));

                                //set it to be permanently part of the route
                                if(mode==Mode.RECHARGE)
                                    tmpRoute2.setRechargeStation(batteryCapacity,rechargeTime,mode,rechargeRate,tmpRoute2.getCustomers().size()-1,false);
                                else
                                    tmpRoute2.setRechargeStation(batteryCapacity,rechargeTime,mode,rechargeRate,tmpRoute2.getCustomers().size()-1,true);

                                //set start of the new segment (position of currently inserted recharge station)

                                if (customerMinus != null) {
                                    tmpRoute2.addCustomer(customerMinus);
                                    tmpRoute2.setSegment(tmpRoute2.getCustomers().size() - 2);
                                }
                                else {
                                    tmpRoute2.setSegment(tmpRoute2.getCustomers().size() - 1);
                                }

                                //if old segment part is feasible

                                if(ConstraintsChecker.isFeasible(tmpRoute2,mode,batteryCapacity)) {

                                    //customer's pickup and delivery requests were successfully scheduled, add this solution to InsertionEvaluator

                                    evaluator.addFeasibleOptimization(tmpRoute2);

                                    if(prints) {
                                        System.out.println("Inserting pickup customer BEFORE set station and delivery customer AFTER set station with naive strategy: " + tmpRoute2 + ", route before change applied: " + route);
                                    }
                                }

                                // b) setting recharge station by the end of the route and adding c+,c- in that order (or one of them if only one exists)
                                else{
                                    //start from route we are trying to insert c+,c- (since even the route with c+ added was infeasible)
                                    tmpRoute2 = new Route(route);

                                    //add a dummy unfixed station at the end of the list
                                    tmpRoute2.addCustomer(new Customer("R - 0.0", 0.0, 0.0, new Pair<Double>(0.0, 0.0), new Pair<Double>(0.0, 0.0), true));

                                    //set it to how it was supposed to be if it will need to be fixed

                                    if(mode==Mode.RECHARGE) {
                                        tmpRoute2.setRechargeStation(batteryCapacity, rechargeTime, mode, rechargeRate, tmpRoute2.getCustomers().size() - 1, false);
                                    }
                                    else {
                                        tmpRoute2.setRechargeStation(batteryCapacity, rechargeTime, mode, rechargeRate, tmpRoute2.getCustomers().size() - 1, true);
                                    }

                                    //add c+ after it

                                    tmpRoute2.addCustomer(customerPlus);

                                    //depending on whether c- exists or not, add them after c+ and set segment index accordingly (indicating position of last charging station and the beginning of a new route segment to check)

                                    if (customerMinus != null) {
                                        tmpRoute2.addCustomer(customerMinus);
                                        tmpRoute2.setSegment(tmpRoute2.getCustomers().size() - 3);
                                    }
                                    else {
                                        tmpRoute2.setSegment(tmpRoute2.getCustomers().size() - 2);
                                    }

                                    //if old segment is feasible

                                    if(ConstraintsChecker.isFeasible(tmpRoute2,mode,batteryCapacity)){

                                        //customer's pickup and delivery requests were successfully scheduled, add this solution to InsertionEvaluator

                                        evaluator.addFeasibleOptimization(tmpRoute2);

                                        if(prints) {
                                            System.out.println("Inserting pickup and delivery customer AFTER set station with naive strategy: " + tmpRoute2 + ", route before change applied: " + route);
                                        }
                                    }
                                }
                            }

                            //c+ does not exist at all, therefore we will work with c- only.
                            //only possible feasibility on this case is setting a station by the end of the list, adding c- after it and ensuring a dummy station after c- is reachable

                            else if(customerMinus!=null){
                                //start with old route
                                tmpRoute2 = new Route(tmpRoute);

                                //add a dummy unfixed station at the end of the list
                                tmpRoute2.addCustomer(new Customer("R - 0.0", 0.0, 0.0, new Pair<Double>(0.0, 0.0), new Pair<Double>(0.0, 0.0), true));

                                //set it to how it was supposed to be if it will need to be fixed

                                if(mode==Mode.RECHARGE) {
                                    tmpRoute2.setRechargeStation(batteryCapacity, rechargeTime, mode, rechargeRate, tmpRoute2.getCustomers().size() - 1,false);
                                }
                                else{
                                    tmpRoute2.setRechargeStation(batteryCapacity, rechargeTime, mode, rechargeRate, tmpRoute2.getCustomers().size() - 1,true);
                                }

                                //add c-
                                tmpRoute2.addCustomer(customerMinus);

                                //set segment's position
                                tmpRoute2.setSegment(tmpRoute2.getCustomers().size()-1);

                                //customer's pickup and delivery requests were successfully scheduled, add this solution to InsertionEvaluator
                                evaluator.addFeasibleOptimization(tmpRoute2);
                            }
                        }
                    }
                    else if(mode==Mode.NO_RECHARGE&&ConstraintsChecker.isFeasible(tmpRoute2,mode,batteryCapacity)) {//if the insertion is feasible and adding the customers in the route places them in a position such that timeAddedToProblem<=arrival time
                        evaluator.addFeasibleOptimization(tmpRoute2);//customer's pickup and delivery requests were successfully scheduled, add this solution to InsertionEvaluator
                    }
                }
            }
        }

        bestInsertion=evaluator.getBestOptimization();

        return bestInsertion;
    }

    //returns average route length for current instance
    
    public static Double getAverageRouteLength(List<Route> fleet){
    	Double sum = 0.0;
    	for(Route route : fleet){
    		sum += route.getRouteLength();
    	}
    	return sum / fleet.size();
    }

    //returns furthest distance between a customer in all routes and a charging station (among all of them)

    public static Double getFurthestDistance(LinkedList<Route> fleet,List<Pair<Double>> chargingStations){
        boolean entered=false;//true iff maxDist was initiated in the loops
        Double maxDist=0.0;//max distance between a customer and a charging station
        for(Route r:fleet){//for every route
            for(Customer c:r.getCustomers()){//for every customer in route r
                if(!Pattern.compile("R.*").matcher(c.getName()).find()) {
                    for (Pair<Double> charginStationLocation : chargingStations) {//for every charging station
                        if (!entered) {//if maxDist has not yet been initialized in the loops
                            entered = true;//set initialized condition to true
                            maxDist = Utilities.getDistance(c.getLocation(), charginStationLocation);//and set maxDist as the first distance pair
                        } else if (Utilities.getDistance(c.getLocation(), charginStationLocation) > maxDist) {//if a longer distance was found
                            maxDist = Utilities.getDistance(c.getLocation(), charginStationLocation);//set maxDist accordingly
                        }
                    }
                }
            }
        }

        return maxDist;
    }

    //returns average customer service time

    public static Double getAverageCustomerService(LinkedList<Route> fleet){
        Double sum=0.0;
        Double customersCnt=0.0;

        for(Route r:fleet){//for every route
            for(Customer c:r.getCustomers()){//for every delivery customer
                if(!c.isPickup()){
                    sum+=r.getMargins().get(r.getCustomers().indexOf(c)).getFirst()-c.getAddedToProblem();//increase sum by a factor of (arrival time - addedToProblem)
                    customersCnt++;//increase customers' counter by 1
                }
            }
        }

        return customersCnt>0?sum/customersCnt:0;//return average if at least one customer exists, otherwise return 0 to avoid division by 0
    }
	
}
