package VRPPD.insertion;

import VRPPD.checks.ConstraintsChecker;
import edu.uci.ics.jung.graph.util.Pair;

import java.util.LinkedList;
import java.util.regex.Pattern;
import VRPPD.onlineAlgo.OnlineVRPPD.Mode;
import org.apache.log4j.Logger;

public class Route {

	Logger LOGGER = Logger.getLogger(Route.class);

    static boolean prints=false;

    private LinkedList<Customer> customers;//list of requests (customers can be either pickup requests or delivery)
    private LinkedList<Pair<Double>> margins;//list of margins for every customer in the form [arrivalTime,waitingTime]
    private Pair<Double> location;//current location of vehicle
    private int capacity;//vehicle capacity
    private int pos;//index in customer list indicating the position where unserviced customers begin
    private Double curTime;//time vehicle added to problem
    private int segment;//current segment unfixed recharging station runs from

    //constructors

    public Route(Pair<Double> location,int capacity,Double curTime){
        this.customers=new LinkedList<Customer>();
        this.margins=new LinkedList<Pair<Double>>();
        this.location=new Pair<Double>(location);
        this.capacity=capacity;
        this.pos=0;
        this.curTime=curTime;

        this.segment=-1;
    }

    public Route(Route route){
        this.customers=new LinkedList<Customer>(route.customers);
        this.margins=new LinkedList<Pair<Double>>(route.margins);
        this.location=new Pair<Double>(route.location);
        this.capacity=route.capacity;
        this.pos=route.pos;
        this.curTime=route.curTime;

        this.segment=route.segment;
    }

    //methods to add customers (note that customer insertions for Plus and Minus of the same customer must be handled SEPARATELY)

    public void addCustomerTo(Customer customer,int i){
        if(capacity!=-1){
            if(!Pattern.compile("R.*").matcher(customer.getName()).find()&&customer.isPickup()){//recharging stations do not effect capacity
                capacity--;
            }
        }

        this.customers.add(i,customer);
        this.margins.add(i,new Pair<Double>(0.0,0.0));
        recalculateMargins(i);
    }

    public void addCustomer(Customer customer){
        if(capacity!=-1){
            if(!Pattern.compile("R.*").matcher(customer.getName()).find()&&customer.isPickup()){//recharging stations do not effect capacity
                capacity--;
            }
        }

        this.customers.add(customer);
        this.margins.add(new Pair<Double>(0.0,0.0));
        recalculateMargins(this.customers.size());
    }

    //methods to remove customers (note that customer removals for Plus and Minus of the same customer must be handled SEPARATELY)

    /*

    public Customer removeCustomer(Customer customer){
        if(capacity!=-1){
            if(!Pattern.compile("R.*").matcher(customer.getName()).find()&&customer.isPickup()){//recharging stations do not effect capacity
                capacity++;
            }
        }

        int pos=customers.indexOf(customer);
        customers.remove(pos);
        margins.remove(pos);

        recalculateMargins(pos);
        return customer;
    }

    */

    public Customer removeCustomerFrom(Customer customer, int pos){
        if(capacity!=-1){
            if(!Pattern.compile("R.*").matcher(customer.getName()).find()&&customer.isPickup()){//recharging stations do not effect capacity
                capacity++;
            }
        }

        customers.remove(pos);
        margins.remove(pos);

        recalculateMargins(pos);
        return customer;
    }

    public void removeLastCustomer(){
        if(capacity!=-1){
            if(!Pattern.compile("R.*").matcher(customers.getLast().getName()).find()&&customers.getLast().isPickup()){//recharging stations do not effect capacity
                capacity++;
            }
        }

        customers.removeLast();
        margins.removeLast();
    }

    //updates waiting/arrival times for all customers in the route

    public void recalculateMargins(int pos){
    	//LOGGER.info("Recalculating margins: " + pos+" "+customers);
        for(int i=-1+(pos-1);i<this.customers.size()-1;i++){
            if(i==-2)
                continue;
            Double curArrival;
            Double curWaiting;

            Double arrival;
            Double waiting;
            Double service;

            Double dist;
            if(i==-1){
                Customer c1=this.customers.getFirst();

                if(prints) {
                    LOGGER.info("updating margins for: " + c1);
                }

                //it's important to notice that arrival to the first customer is dependent on its distance from the vehicle AND the time the vehicle was added to the problem

                curArrival=Utilities.getDistance(this.getLocation(),c1.getLocation())+curTime;

                //assert(curArrival>=c1.getAddedToProblem());

                if(curArrival<c1.getAddedToProblem()) {
                    curArrival = c1.getAddedToProblem();
                    if(prints) {
                        LOGGER.info("Customer's margin must be moved so that arrival time>=timeAddedToProblem!");
                    }
                }

                if(prints){
                    LOGGER.info("Distance: " + Utilities.getDistance(this.getLocation(),c1.getLocation()));
                }

                Pair<Double> timeWindow=c1.getTimeWindow();
                if(timeWindow.getFirst()<=curArrival){
                    curWaiting=0.0;
                }
                else{
                    curWaiting=timeWindow.getFirst()-curArrival;
                }
                margins.set(i+1,new Pair<Double>(curArrival,curWaiting));
                if(prints) {
                    LOGGER.info("set margins to:" + margins.get(i + 1));
                    LOGGER.info("slack time is: " + getCustomerSlackTime(i+1));
                }
            }
            else{
                Customer c1=this.customers.get(i);
                Customer c2=this.customers.get(i+1);

                if(prints){
                    LOGGER.info("updating margins for: " + c1 + " and " + c2);
                }

                arrival=margins.get(i).getFirst();
                waiting=margins.get(i).getSecond();
                service=c1.getServiceTime();
                dist= Utilities.getDistance(c1.getLocation(),c2.getLocation());

                curArrival=arrival+waiting+service+dist;

                //assert(curArrival>=c2.getAddedToProblem());

                if(curArrival<c2.getAddedToProblem()) {
                    curArrival = c2.getAddedToProblem();
                    if(prints){
                        LOGGER.info("Customer's margin must be moved so that arrival time>=timeAddedToProblem!");
                    }
                }

                //LOGGER.info("Distance: " + curArrival);

                Pair<Double> timeWindow=c2.getTimeWindow();
                if(timeWindow.getFirst()<=curArrival){
                    curWaiting=0.0;
                }
                else{
                    curWaiting=timeWindow.getFirst()-curArrival;
                }
                margins.set(i+1,new Pair<Double>(curArrival,curWaiting));
                if(prints) {
                    LOGGER.info("set margins to:" + margins.get(i + 1));
                    LOGGER.info("slack time is: " + getCustomerSlackTime(i+1));
                }
            }
        }

    }

    //calculates total route length

    public Double getRouteLength(){
        LinkedList<Customer> requests=new LinkedList<Customer>(this.customers);
        Double cost=0.0;
        for(int i=-1;i<requests.size()-1;i++) {
            Pair<Double> p1;
            if(i==-1){
                p1 = location;
            }
            else {
                p1 = requests.get(i).getLocation();
            }
            Pair<Double> p2;
            p2 = requests.get((i + 1)).getLocation();
            cost = cost + Utilities.getDistance(p1,p2);
        }
        return cost;
    }

    //calculates total slack time (limit of time window - time of vehicle arrival for every customer) for the route

    public Double getSlackTimes(){
        Double totalSlack=0.0;
        for(Customer customer:customers){
            totalSlack+=getCustomerSlackTime(customer);
        }

        return totalSlack;

    }

    //returns customer's slack time (limit of time window - time of vehicle arrival)

    public Double getCustomerSlackTime(Customer customer){
        return customers.get(customers.indexOf(customer)).getTimeWindow().getSecond()-margins.get(customers.indexOf(customer)).getFirst();
    }

    public Double getCustomerSlackTime(int customer){
    	//LOGGER.info("TW: " + customers.get(customer).getTimeWindow().getSecond());
    	//LOGGER.info("M: " + margins.get(customer).getFirst());
        return customers.get(customer).getTimeWindow().getSecond()-margins.get(customer).getFirst();
    }

    /*

    //returns request's total slack time (limit of time window - time of vehicle arrival for pickup and corresponding delivery)

    public Double getRequestSlackTime(Customer customer){
        Customer c1,c2;
        c1=c2=null;
        for(Customer c:customers){//find customerPlus and customerMinus
            if(c.getName().equals(customer.getName())){
                if(c.isPickup()){
                    c1=c;
                }
                else{
                    c2=c;
                }
            }
        }
        if(c1==null){//the request was simply a delivery
            return getCustomerSlackTime(c2);
        }
        else if(c2==null){//the request was simply a pickup
            return getCustomerSlackTime(c1);
        }
        else{//normal request, calculate its total slack time
            return getCustomerSlackTime(c1)+getCustomerSlackTime(c2);
        }
    }

    */

    //moves customers' list index so that we know where to check for unserviced customers

    public void updateServiced(int curTime){
        if(pos!=customers.size()) {//if index has not already reached the end of the list
            if (margins.get(pos).getFirst() <= curTime) {//if first unserviced customer must be serviced, start servicing them and advance the index by 1
                pos++;
            }
        }
    }

    /*

    //updates vehicle's location

    public void setLocation(Pair<Double> location){
        this.location=new Pair<Double>(location);
    }

    //sets index position that separates route between fixed and unfixed section

    public void setIndex(int pos){
        this.pos=pos;
    }

    */

    //sets index of the current segment the battery needs to last for (segment position in the list is the last recharging station fixed in the route)

    public void setSegment(int pos){
        this.segment=pos;
    }


    //updates recharging station values
    //template is the last customer in the list, who will lend most of its stats like position, service time etc
    //this is necessary since all loops in cheapest insertion logic calculate everything for all customers.

    //if template parameter is not null, then the station is essentially a dummy, not effecting route stats and existing simply for unified algorithmic handling.
    //null template parameter indicates the station will need to be properly fixed and set in the route

    public void setRechargeStation(Double batteryCapacity,Double rechargeTime,Mode mode,Double recharge,int pos,boolean advancedFull){

        Customer station=customers.get(pos);//get last customer in the route (unfixed station to set)

        station.setClosestStationToStation();

        Customer lastCustomer=customers.get(pos-1);//get next-to-last customer in the route

        station.setLocation(lastCustomer.getClosestStation());//set recharghing location to the closest recharging station to lastCustomer
        station.setAddedToProblem(lastCustomer.getAddedToProblem());//station's addedToProblem is borrowed from lastCustomer

        //set station's time window as arrival of lastCustomer plus lastCustomer's waiting plus lastCustomer's service plus distance from lastCustomer in order to ensure no time window violations occur
        //also, set both start and end of its time window to the same value to ensure the station will always be set at the position it currently is

        Double timeWindowLimit=margins.get(customers.indexOf(lastCustomer)).getFirst()+Utilities.getDistance(lastCustomer.getLocation(),station.getLocation())+lastCustomer.getServiceTime()+margins.get(customers.indexOf(lastCustomer)).getSecond();

        Double length=getSegmentLength();

        if((mode==Mode.ADVANCED_RECHARGE||mode==Mode.ULTRA_NAIVE)&&!advancedFull) {

            //value with battery left from last station
            Double lastStationBattery = -1.0;

            //locate last station before current one
            for (int i = 0; i < pos; i++) {
                if (Pattern.compile("R.*").matcher(customers.get(i).getName()).find()) {
                    String[] stationName = customers.get(i).getName().split(" ");
                    lastStationBattery = Double.parseDouble(stationName[4]);
                }
            }

            //if no other station exists in the route (meaning this is the first station to add since vehicle left the depot)
            if (lastStationBattery == -1.0) {
                //vehicles start with full capacity,therefore output the corresponding value
                Double newCapacity = batteryCapacity - length + recharge * batteryCapacity;

                if (newCapacity > batteryCapacity) {
                    station.setName(String.format("R - %s - %s - %s", (length), batteryCapacity,(batteryCapacity-length)));
                }
                else{
                    station.setName(String.format("R - %s - %s - %s", (length), newCapacity,(batteryCapacity-length)));
                }

                //set time the vehicle needs to refuel

                assert batteryCapacity > 0;

                //recharge x% of the battery, or as much as it needs if it is fuller than expected

                if (newCapacity > batteryCapacity) {
                    station.setServiceTime(length / batteryCapacity * rechargeTime);
                } else {
                    station.setServiceTime(rechargeTime * recharge);
                }
            } else {
                //found another recharge before current one
                //the battery the vehicle will use for the upcoming segment is the difference of the battery consumed to get here from the initial battery plus 30% of original capacity from recharging

                Double newCapacity = lastStationBattery - length + recharge * batteryCapacity;
                if (newCapacity > batteryCapacity) {
                    station.setName(String.format("R - %s - %s - %s", (length), batteryCapacity,(lastStationBattery-length)));
                }
                else{
                    station.setName(String.format("R - %s - %s - %s", (length), newCapacity,(lastStationBattery-length)));
                }

                //set time the vehicle needs to refuel

                assert batteryCapacity > 0;

                //recharge 30% of the battery, or as much as it needs if it is fuller than expected

                if (newCapacity > batteryCapacity) {
                    station.setServiceTime((batteryCapacity-lastStationBattery+length) / batteryCapacity *rechargeTime);
                } else {
                    station.setServiceTime(rechargeTime * recharge);
                }
            }

            station.setTimeWindow(new Pair<Double>(timeWindowLimit,timeWindowLimit));

        }
        else if(mode==Mode.ADVANCED_RECHARGE||mode==Mode.ULTRA_NAIVE){
            //value with battery left from last station
            Double lastStationBattery = -1.0;

            //locate last station before current one
            for (int i = 0; i < pos; i++) {
                if (Pattern.compile("R.*").matcher(customers.get(i).getName()).find()) {
                    String[] stationName = customers.get(i).getName().split(" ");
                    lastStationBattery = Double.parseDouble(stationName[4]);
                }
            }

            //if no other station exists in the route (meaning this is the first station to add since vehicle left the depot)
            if (lastStationBattery == -1.0) {
                //vehicles start with full capacity,therefore output the corresponding value
                Double newCapacity = batteryCapacity - length + 1.0 * batteryCapacity;

                if (newCapacity > batteryCapacity) {
                    station.setName(String.format("R - %s - %s - %s", (length), batteryCapacity,(batteryCapacity-length)));
                }
                else{
                    station.setName(String.format("R - %s - %s - %s", (length), newCapacity,(batteryCapacity-length)));
                }

                //set time the vehicle needs to refuel

                assert batteryCapacity > 0;

                //recharge x% of the battery, or as much as it needs if it is fuller than expected

                if (newCapacity > batteryCapacity) {
                    station.setServiceTime(length / batteryCapacity * rechargeTime);
                } else {
                    station.setServiceTime(rechargeTime);
                }
            } else {
                //found another recharge before current one
                //the battery the vehicle will use for the upcoming segment is the difference of the battery consumed to get here from the initial battery plus 30% of original capacity from recharging

                Double newCapacity = lastStationBattery - length + 1.0 * batteryCapacity;
                if (newCapacity > batteryCapacity) {
                    station.setName(String.format("R - %s - %s - %s", (length), batteryCapacity,(lastStationBattery-length)));
                }
                else{
                    station.setName(String.format("R - %s - %s - %s", (length), newCapacity,(lastStationBattery-length)));
                }

                //set time the vehicle needs to refuel

                assert batteryCapacity > 0;

                //recharge 30% of the battery, or as much as it needs if it is fuller than expected

                if (newCapacity > batteryCapacity) {
                    station.setServiceTime((batteryCapacity-lastStationBattery+length) / batteryCapacity *rechargeTime);
                } else {
                    station.setServiceTime(rechargeTime);
                }
            }

            station.setTimeWindow(new Pair<Double>(timeWindowLimit,timeWindowLimit));
        }
        else if(mode==Mode.RECHARGE){
            //set station's name to station - distance driven (needed to compare batteryCapacity to that in rechargingConstraint)

            station.setName(String.format("R - %s",(length)));

            station.setTimeWindow(new Pair<Double>(timeWindowLimit,timeWindowLimit));

            //set time the vehicle needs to refuel

            assert batteryCapacity>0;

            station.setServiceTime((length/batteryCapacity)*rechargeTime);
        }

        station.setPickup(true);

        recalculateMargins(pos);
    }

    //returns route with a set recharge station at the end iff the candidate station is "close enough" to the last customer or null if conditions are not satisfied

    public Route tryStationInsertion(Double minChargeRate,Double chargeRate,Double batteryCapacity,Double rechargeTime,Double rechargeRate,Mode mode,Double acceptableDist,Customer lastInserted){
        Route route=new Route(this);

        if (route.getSegmentLength(route.customers.indexOf(lastInserted))+Utilities.getDistance(lastInserted.getLocation(),lastInserted.getClosestStation()) >= minChargeRate * batteryCapacity && route.getSegmentLength(route.customers.indexOf(lastInserted))+Utilities.getDistance(lastInserted.getLocation(),lastInserted.getClosestStation()) <= chargeRate * batteryCapacity) {

            //add a dummy unfixed station at the end of the list
            route.addCustomerTo(new Customer("R - 0.0", 0.0, 0.0, new Pair<Double>(0.0, 0.0), new Pair<Double>(0.0, 0.0), true),route.customers.indexOf(lastInserted)+1);

            //set it to how it was supposed to be
            route.setRechargeStation(batteryCapacity, rechargeTime, mode, rechargeRate,route.customers.indexOf(lastInserted)+1,false);

            route.setSegment(route.customers.indexOf(lastInserted)+1);

            Customer station;
            station = route.getCustomers().get(route.customers.indexOf(lastInserted)+1);

            //if station is "close enough" to the customer and feasible, add it

            if (Utilities.getDistance(lastInserted.getLocation(), station.getLocation()) <= acceptableDist) {
                if(ConstraintsChecker.isFeasible(route,mode,batteryCapacity)){
                    return route;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    //returns number of stations in route

    public int getNumberOfStations(){
        int cnt=0;
        for(Customer c:customers){
            if(Pattern.compile("R.*").matcher(c.getName()).find()){
                cnt++;
            }
        }
        return cnt;
    }

    //returns length of route's segment starting from segment data member

    private Double getSegmentLength(){
        LinkedList<Customer> requests=new LinkedList<Customer>(this.customers);
        Double cost=0.0;
        for(int i=segment;i<requests.size()-1;i++) {
            Pair<Double> p1;
            if(i==-1){
                p1 = location;
            }
            else {
                p1 = requests.get(i).getLocation();
            }
            Pair<Double> p2;
            p2 = requests.get((i + 1)).getLocation();
            if(Pattern.compile("R.*").matcher(requests.get(i+1).getName()).find()&&i!=segment){
                return cost + Utilities.getDistance(p1,p2);
            }
            cost = cost + Utilities.getDistance(p1,p2);
        }
        return cost;
    }

    private Double getSegmentLength(int limit){
        LinkedList<Customer> requests=new LinkedList<Customer>(this.customers);
        Double cost=0.0;
        for(int i=segment;i<limit;i++) {
            Pair<Double> p1;
            if(i==-1){
                p1 = location;
            }
            else {
                p1 = requests.get(i).getLocation();
            }
            Pair<Double> p2;
            p2 = requests.get((i + 1)).getLocation();
            if(Pattern.compile("R.*").matcher(requests.get(i+1).getName()).find()&&i!=segment){
                return cost + Utilities.getDistance(p1,p2);
            }
            cost = cost + Utilities.getDistance(p1,p2);
        }
        return cost;
    }

    //accesssors

    public LinkedList<Customer> getCustomers(){
        return customers;
    }

    public LinkedList<Pair<Double>> getMargins(){
        return margins;
    }

    public Pair<Double> getLocation(){
        return location;
    }

    public int getCapacity(){
        return capacity;
    }

    public int getIndex(){
        return pos;
    }

    public int getSegment(){
        return segment;
    }

    //output

    @Override

    public String toString(){
        return (this.getCustomers().toString());
    }

}
