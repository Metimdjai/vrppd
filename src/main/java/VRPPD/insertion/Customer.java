package VRPPD.insertion;

import edu.uci.ics.jung.graph.util.Pair;

import java.util.List;


public class Customer {

    private String name;//customer's name
    private Double addedToProblem;//time customer was added to the problem
    private Double serviceTime;//service time (which can be a pickup or a delivery)
    private Pair<Double> location;//location of customer
    private Pair<Double> timeWindow;//time window for the request (which can be a pickup or a delivery)
    private boolean isPickup;//true if customer request is a pickup and false if it is a delivery
    private Pair<Double> closestStation;

    //constructors

    public Customer(String name, Double addedToProblem, Double serviceTime, Pair<Double> location, Pair<Double> timeWindow, boolean isPickup){
        this.name=name;
        this.addedToProblem=addedToProblem;
        this.serviceTime=serviceTime;
        this.location=new Pair<Double>(location);
        this.timeWindow=new Pair<Double> (timeWindow);
        this.isPickup=isPickup;
        this.closestStation=null;
    }

    /*

    public Customer(Customer customer){
        name=customer.name;
        addedToProblem=customer.addedToProblem;
        serviceTime=customer.serviceTime;
        location=customer.location;
        timeWindow=customer.timeWindow;
        isPickup=customer.isPickup;
        closestStation=customer.closestStation;
    }

    */

    //returns true iff customer is a pickup request

    public boolean isPickup(){
        return isPickup;
    }

    //sets closest charging station to Customer

    public void setClosestStation(List<Pair<Double>> chargingStations){
        Double minDist=0.0;
        boolean entered=false;
        for(Pair<Double> station:chargingStations){
            if(!entered){
                entered=true;
                minDist=Utilities.getDistance(location,station);
                closestStation=station;
            }
            else if(minDist>Utilities.getDistance(location,station)){
                minDist=Utilities.getDistance(location,station);
                closestStation=station;
            }
        }
    }

    //sets closest station to self if customer is a station

    public void setClosestStationToStation(){
        closestStation=location;
    }

    //mutators

    public void setName(String name){
        this.name=name;
    }

    public void setAddedToProblem(Double addedToProblem){
        this.addedToProblem=addedToProblem;
    }

    public void setServiceTime(Double serviceTime){
        this.serviceTime=serviceTime;
    }

    public void setLocation(Pair<Double> location){
        this.location=new Pair<Double>(location);
    }

    public void setTimeWindow(Pair<Double> timeWindow){
        this.timeWindow=timeWindow;
    }

    public void setPickup(boolean isPickup){
        this.isPickup=isPickup;
    }

    //accessors

    public String getName(){
        return name;
    }

    public Double getAddedToProblem(){
        return addedToProblem;
    }

    public Double getServiceTime(){
        return serviceTime;
    }

    public Pair<Double> getLocation(){
        return location;
    }

    public Pair<Double> getTimeWindow(){
        return timeWindow;
    }

    public Pair<Double> getClosestStation(){
        return closestStation;
    }

    //output

    @Override

    public String toString(){
        return isPickup()?(name+"+:"+location):(name+"-:"+location);
        //return name;
    }
}
