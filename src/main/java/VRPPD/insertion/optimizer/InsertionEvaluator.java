package VRPPD.insertion.optimizer;


import VRPPD.insertion.Route;

import java.util.LinkedList;

public class InsertionEvaluator {

    private LinkedList<Route> feasibleOptimizations;//list of feasible optimizations

    //constructor

    public InsertionEvaluator(){
        feasibleOptimizations=new LinkedList<Route>();
    }

    //add candidate to feasibleOptimizations

    public void addFeasibleOptimization(Route route){
        feasibleOptimizations.add(route);
    }

    //returns best possible optimization (or null if none exist)

    public Route getBestOptimization(){
        boolean entered=false;
        Route bestInsertion=null;
        for(Route candidate:feasibleOptimizations){
            if(!entered){
                entered=true;
                bestInsertion=candidate;
            }
            else if(improvementOf(bestInsertion,candidate)){
                bestInsertion=candidate;
            }
        }
        return bestInsertion;
    }

    //checks whether candidate route is an improvement over the current best. Comparison criterion will probably need to be changed

    private boolean improvementOf(Route best,Route cand){
        assert best!=null&&cand!=null;
        return best.getRouteLength()>=cand.getRouteLength();
    }

    //output

    @Override

    public String toString(){
        return ("["+feasibleOptimizations+","+getBestOptimization()+"]");
    }
}
