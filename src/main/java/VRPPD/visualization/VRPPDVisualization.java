package VRPPD.visualization;

import VRPPD.insertion.Customer;
import VRPPD.insertion.Route;
import edu.uci.ics.jung.graph.util.Pair;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class VRPPDVisualization {

    Logger LOGGER = Logger.getLogger(VRPPDVisualization.class);
    private LinkedList<Route> fleet;//list of routes (solution of current problem)
    private List<Pair<Double>> chargingStations;//list of charging stations
    private String path;//path of input file (needed to output the results to the same location)
    private boolean mode;//true iff temporary files are to be deleted after execution

    public VRPPDVisualization(LinkedList<Route> fleet,List<Pair<Double>> chargingStations,String path,boolean mode){
        this.fleet=new LinkedList<Route>(fleet);
        this.path=path;
        this.mode=mode;
        this.chargingStations=chargingStations;
    }

    public void visualize(){

        String solutionOutput="";
        String stationsOutput="";

        //for every vehicle in fleet
        for(Route r:fleet){

            Double colour =Math.random();

            solutionOutput+=String.format(Locale.ENGLISH,"D %.10f %.10f %.3f\n",0.0,0.0,colour);//add default location of vehicles (assuming all start from the same depot)

            for(int i=0;i<r.getCustomers().size();i++){
                Customer c;
                //get customer nodes
                c=r.getCustomers().get(i);

                //add points of route coordinates to data file
                if(c.isPickup()) {
                    if(!Pattern.compile("R.*").matcher(c.getName()).find())
                        solutionOutput += String.format(Locale.ENGLISH, "%s+ %.10f %.10f %.3f\n", c.getName(), c.getLocation().getFirst(), c.getLocation().getSecond(), colour);
                    else
                        solutionOutput += String.format(Locale.ENGLISH, "%s %.10f %.10f %.3f\n", "R", c.getLocation().getFirst(), c.getLocation().getSecond(), colour);
                }
                else
                    solutionOutput+=String.format(Locale.ENGLISH,"%s- %.10f %.10f %.3f\n",c.getName(),c.getLocation().getFirst(),c.getLocation().getSecond(),colour);

            }

            solutionOutput+="\n";
        }


        if(chargingStations!=null) {
            stationsOutput += "# X Y\n";
            for (Pair<Double> station : chargingStations) {
                stationsOutput += String.format(Locale.ENGLISH, "R %.10f %.10f\n", station.getFirst(), station.getSecond());
            }
        }

        File gnuPlotData;//file with the gnuplot data
        FileWriter fw;//corresponding file writer and buffered writer
        BufferedWriter bw;

        File stationsData = null;//file with the gnuplot data
        FileWriter fw2;//corresponding file writer and buffered writer
        BufferedWriter bw2;

        File gnuPlotScript;//file with the GnuPlot script
        FileWriter fw3;//corresponding file writer and buffered writer
        BufferedWriter bw3;

        try {
            gnuPlotData = File.createTempFile(path,".dat",new File(path.substring(0,path.lastIndexOf('/'))));//create new temporary file with .dat extension (the GnuPlot data file)

            fw = new FileWriter(gnuPlotData);
            bw = new BufferedWriter(fw);

            //write stream with points to .dat file
            bw.append(solutionOutput);

            //close output stream
            bw.close();
            fw.close();

            if(chargingStations!=null) {

                stationsData = File.createTempFile(path, ".stations.dat", new File(path.substring(0, path.lastIndexOf('/'))));//create new temporary file with .dat extension (the GnuPlot data file)

                fw2 = new FileWriter(stationsData);
                bw2 = new BufferedWriter(fw2);

                //write stream with points to .dat file
                bw2.append(stationsOutput);

                //close output stream
                bw2.close();
                fw2.close();

            }

            gnuPlotScript=File.createTempFile(path,".p",new File(path.substring(0,path.lastIndexOf('/'))));//create new temporary file with .p extension (the GnuPlot script file)

            fw3 = new FileWriter(gnuPlotScript);
            bw3 = new BufferedWriter(fw3);

            //write the GnuPlot script adding the .dat and .png paths
            bw3.append("unset colorbox\n" + "set autoscale\n" + "set title \"VRPPD Visualization\"\n" + "set terminal x11 0 persist enhanced font \"Helvetica,20\"\n").append(String.format("\nplot '%s' u 2:3:4 notitle w l lc palette,",gnuPlotData.getAbsolutePath()));
            bw3.append(String.format("'%s' u 2:3:1 with labels notitle\n",gnuPlotData.getAbsolutePath()));


            if(chargingStations!=null) {
                assert  stationsData!=null;
                bw3.append("set terminal x11 1 persist enhanced font \"Helvetica,20\"\n");
                bw3.append(String.format("\nplot '%s' u 2:3:1 with labels notitle", stationsData.getAbsolutePath()));
            }

            //close output stream
            bw3.close();
            fw3.close();

            //start a new process to execute the GnuPlot script via gnuplot utility
            Runtime r = Runtime.getRuntime();
            Process p = r.exec(String.format("gnuplot %s",gnuPlotScript.getAbsolutePath()));

            try {
                p.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(mode){//if temporary files are to be deleted, delete them on exit
                gnuPlotData.deleteOnExit();
                if(chargingStations!=null) {
                    assert stationsData!=null;
                    stationsData.deleteOnExit();
                }
                gnuPlotScript.deleteOnExit();
            }
            else{
                //print output paths so that user can find the files afterwards

                LOGGER.info("Gnuplot data saved at: "+ gnuPlotData.getAbsolutePath());
                LOGGER.info("Gnuplot script saved at: "+gnuPlotScript.getAbsolutePath());
                if(chargingStations!=null){
                    assert stationsData!=null;
                    LOGGER.info("Gnuplot stations' data saved at: "+stationsData.getAbsolutePath());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
