package VRPPD.onlineAlgo;

//import VRPPD.charging.ChargingUtilities;
import VRPPD.charging.ChargingUtilities;
import VRPPD.checks.ConstraintsChecker;
import VRPPD.input.Reader;
import VRPPD.insertion.Customer;
import VRPPD.insertion.CustomerRequest;
import VRPPD.insertion.Route;
import VRPPD.insertion.Utilities;
//import VRPPD.onlineAlgo.AdvancedDynamicWaiting.ADW;
//import VRPPD.onlineAlgo.Zoning.RouteZoner;

import VRPPD.visualization.VRPPDVisualization;

import edu.uci.ics.jung.graph.util.Pair;
import org.apache.log4j.Logger;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class OnlineVRPPD {

    Logger LOGGER = Logger.getLogger(OnlineVRPPD.class);
    
    public enum Mode {NO_RECHARGE, RECHARGE, ADVANCED_RECHARGE, ULTRA_NAIVE}

    static boolean prints=false;

    private double batteryCapacity;
    private double rechargeTime;
    private double totalLength;
    private double totalSlack;
    private int vehicles;
    private int recharges;
    private int unserviced;
    private double averageBatteryLeft;

    //onlineVRPPDMode: "recharge" to use basic recharging strategy, "advanced recharge" to use advanced recharging
    //any other string parameter will force default execution (without vehicle recharges)

    public OnlineVRPPD(String path,Mode onlineVRPPDMode,double battery,double recharge){

        int unserviced=0;

        LinkedList<Customer> bannedCustomers=new LinkedList<Customer>();

        //add banned customers here!

        //bannedCustomers.add(new Customer("1",0.0,0.0,new Pair<Double>(0.0,0.0),new Pair<Double>(0.0,0.0),false));

        Double batteryCapacity=0.0,rechargeTime=0.0;

        //recharge rate

        Double rechargeRate=1.0;//rate of battery to recover on each station

        Double chargeRate=0.35;//rate of battery's current charge

        Double minChargeRate=0.0;//least acceptable rate of battery's current charge

        Double acceptableDist=2.0;//max distance that a station can be considered "close enough" to a customer

        if(onlineVRPPDMode==Mode.ULTRA_NAIVE) {
            //rechargeRate = 1.0;
            chargeRate = 1.0;
            //acceptableDist = Double.MAX_VALUE;
        }

        List<Pair<Double>> chargingStations=null;

        Reader reader=new Reader(path);
        LinkedList<Route> fleet=new LinkedList<Route>();//fleet of vehicles

        LinkedList<CustomerRequest> customers = reader.getCustomers();

        Double timeLimit=customers.getLast().getRequest().getFirst().getAddedToProblem();

        //set experimental values

        if(onlineVRPPDMode != Mode.NO_RECHARGE){
            batteryCapacity=battery;
            rechargeTime=recharge;

            chargingStations= ChargingUtilities.getChargingStations(customers);
        }

        //print battery capacity

        if(prints)
            LOGGER.info("Battery capacity: "+ batteryCapacity);

        //print average customer service

        if(prints)
            LOGGER.info("Average customer service: "+rechargeTime);

        fleet.add(new Route(new Pair<Double>(20.0,30.0),reader.getCapacity(),0.0));


        //temporary list holding customers to calculate battery life since customers list gets emptied over time
        LinkedList<CustomerRequest> tmp=new LinkedList<CustomerRequest>(customers);

        //swap for loop lines to do smaller test instead of the whole dataset

        for(int time=0;time<=timeLimit;time++){
        //for(int time=0;time<=22;time++){

            //LOGGER.info("Inserting customers with time in == " + time);

            //advance serviced index in customers' lists for all vehicles
            for (Route route : fleet) {
                //LOGGER.info("Updating routes according to current time");
                route.updateServiced(time);
                //LOGGER.info("Route " + (fleet.indexOf(route) + 1) + " - first available position of insertion of unserviced customers: " + (route.getIndex() + 1));
            }

            while(!customers.isEmpty()&&customers.peek().getRequest().getFirst().getAddedToProblem() == time) {

                //LOGGER.info(customers.peek().getRequest().getFirst().getServiceTime());

                CustomerRequest customer = customers.poll();

                boolean found=false;

                for(Customer c:bannedCustomers){
                    if(c.getName().equals(customer.getRequest().getFirst().getName())){
                        found=true;
                        break;
                    }
                }

                if(found) {
                    continue;
                }

                if (onlineVRPPDMode != Mode.NO_RECHARGE){
                    customer.getRequest().getFirst().setClosestStation(chargingStations);
                    customer.getRequest().getSecond().setClosestStation(chargingStations);
                }

                //LOGGER.info("Servicing customer: " + customer.toString() + " " + customer.getRequest().getFirst().getServiceTime());

                Route unscheduledRoute=null;
                Route oldRoute=null;

                LinkedList<Pair<Route>> feasibleInsertions=new LinkedList<Pair<Route>>();//list of feasible insertions on some of the routes

                for(Route route:fleet){//for every vehicle, try finding the cheapest insertion of customer's request
                    Route feasibleInsertion = Utilities.findBestInsertion(customer.getRequest().getFirst(),customer.getRequest().getSecond(),route,chargingStations,onlineVRPPDMode,batteryCapacity,rechargeTime,rechargeRate);
                    if(feasibleInsertion!=null){//if a feasible cheapest insertion exists for this vehicle, add it to candidates
                        feasibleInsertions.add(new Pair<Route>(feasibleInsertion,route));
                    }
                }

                if(feasibleInsertions.isEmpty()){//no cheapest insertion was feasible for current fleet, we need a new vehicle

                    Route newRoute=new Route(new Pair<Double>(20.0,30.0),reader.getCapacity(),Double.parseDouble(""+time));
                    newRoute.addCustomerTo(customer.getRequest().getFirst(),0);
                    newRoute.addCustomerTo(customer.getRequest().getSecond(),1);

                    if(!ConstraintsChecker.isFeasible(newRoute,Mode.NO_RECHARGE,batteryCapacity)){
                        if(prints)
                            LOGGER.info("Cannot insert customer "+customer+" in a feasible position.");
                        unserviced++;
                    }
                    else {

                        //LOGGER.info("Customer " + customer + " will be added to a new vehicle!");

                        fleet.add(newRoute);
                        //unscheduledRoute = newRoute;
                    }
                }
                else{
                    boolean entered=false;

                    for(Pair<Route> cand:feasibleInsertions){//find the cheapest insertion candidate
                        if(!entered){
                            entered=true;
                            unscheduledRoute=cand.getFirst();
                            oldRoute=cand.getSecond();
                        }
                        if(unscheduledRoute.getRouteLength()-oldRoute.getRouteLength()>cand.getFirst().getRouteLength()-cand.getSecond().getRouteLength()){
                            unscheduledRoute=cand.getFirst();
                            oldRoute=cand.getSecond();
                        }
                    }

                    Route advRoute;

                    assert  unscheduledRoute!=null;

                    //obtained cheapest insertion, now try adding a recharge if conditions are ideal

                    //if strategy is advanced recharge
                    if(onlineVRPPDMode==Mode.ADVANCED_RECHARGE||onlineVRPPDMode==Mode.ULTRA_NAIVE) {
                        advRoute=unscheduledRoute.tryStationInsertion(minChargeRate,chargeRate,batteryCapacity,rechargeTime,rechargeRate,onlineVRPPDMode,acceptableDist,customer.getRequest().getSecond());
//                        advRoute=unscheduledRoute.tryStationInsertion(minChargeRate,chargeRate,batteryCapacity,rechargeTime,rechargeRate,onlineVRPPDMode,acceptableDist,customer.getRequest().getFirst());

                        //if an ideal recharge could be found and no compulsory recharge station was set in the route

                        if(advRoute!=null&&oldRoute.getNumberOfStations()==unscheduledRoute.getNumberOfStations()){

                            if(prints)
                                System.out.println(String.format("(%s) Found feasible closest station to add: " +advRoute+" ,route before change applied: "+oldRoute,onlineVRPPDMode));

                            //set cheapest insertion as the route found with the addition of the ideal recharge
                            fleet.set(fleet.indexOf(oldRoute),advRoute);
                        }
                        else{
                            //one of the conditions was not met, therefore simply add the cheapest insertion

                            fleet.set(fleet.indexOf(oldRoute),unscheduledRoute);
                        }
                    }
                    else{
                        //simply add the cheapest insertion on other strategies

                        fleet.set(fleet.indexOf(oldRoute),unscheduledRoute);
                    }
                }

                /*

                if(unscheduledRoute!=null) {
                    LOGGER.info("Added " + unscheduledRoute);
                    LOGGER.info("Margins " + unscheduledRoute.getMargins());
                    LOGGER.info("Slack Time " + unscheduledRoute.getSlackTimes());
                    LOGGER.info("Route length " + unscheduledRoute.getRouteLength());
                }

                */
            }

        }

        if(prints)
            LOGGER.info("Solution:");

        Double totalSlack=0.0;
        Double totalLength=0.0;
        for(Route route:fleet){
            if(prints)
                LOGGER.info("Route "+(fleet.indexOf(route)+1)+" : "+route+" : "+route.getIndex()+"/"+route.getCustomers().size()+" ,slack time of route: "+route.getSlackTimes()+" ,length of route: "+route.getRouteLength());
            //LOGGER.info("Slack Time "+route.getSlackTimes());
            //LOGGER.info("Route length "+route.getRouteLength());
            totalLength+=route.getRouteLength();
            totalSlack+=route.getSlackTimes();
        }

        if(prints)
            LOGGER.info("\nFinal solution stats\n"+"Number of vehicles: "+fleet.size()+", Total route length: "+totalLength+", Total slack time: "+totalSlack);

        int cnt=0;

        this.averageBatteryLeft=0.0;

        for(Route route:fleet){
        	for(int i=1;i<route.getCustomers().size();i++){
        		double timeBetween = (route.getMargins().get(i).getFirst() - route.getMargins().get(i-1).getFirst());
        		double timeNeeded = Utilities.getDistance(route.getCustomers().get(i).getLocation(), route.getCustomers().get(i-1).getLocation());
        		if(Math.abs(timeNeeded-timeBetween)>0.1){
                    if(prints) {
                        LOGGER.info("wasted time " + Math.abs(timeNeeded - timeBetween) + " " + route.getCustomers().get(i - 1) + " " + route.getCustomers().get(i));
                    }
                }
                if(Pattern.compile("R.*").matcher(route.getCustomers().get(i).getName()).find()){
                    String[] stationName=route.getCustomers().get(i).getName().split(" ");
                    if(onlineVRPPDMode==Mode.RECHARGE){
                        this.averageBatteryLeft+=(batteryCapacity-Double.parseDouble(stationName[2]));
                    }
                    else{
                        this.averageBatteryLeft+=Double.parseDouble(stationName[6]);
                    }
                    cnt++;
                }
        	}
        }

        if(prints)
            LOGGER.info("Number of Recharges in solution: "+cnt);

        //print battery capacity

        if(prints)
            LOGGER.info("Battery capacity: "+ batteryCapacity);

        //print average customer service

        if(prints)
            LOGGER.info("Average customer service: "+rechargeTime);


        if(onlineVRPPDMode==Mode.NO_RECHARGE){//if running at any non-recharging mode, calculate battery capacity and recharge time

            batteryCapacity=ChargingUtilities.getBatteryCapacity(fleet,ChargingUtilities.getChargingStations(tmp));
            rechargeTime=3/2*Utilities.getAverageCustomerService(fleet);
        }

        this.batteryCapacity=batteryCapacity;
        this.rechargeTime=rechargeTime;
        this.totalLength=totalLength;
        this.totalSlack=totalSlack;
        this.vehicles=fleet.size();
        this.recharges=cnt;
        if(this.recharges>0)
            this.averageBatteryLeft/=this.recharges;
        else
            this.averageBatteryLeft=0.0;

        //print battery capacity

        if(prints)
            LOGGER.info("Battery capacity of recharge strategy: "+ batteryCapacity);

        //print average customer service

        if(prints)
            LOGGER.info("Average customer service of recharge strategy: "+rechargeTime);

        //find min,max,average station of a customer from its closest charging station

        Double s=0.0;
        Double maxDist=0.0;
        Double minDist=9999.0;

        if(onlineVRPPDMode!=Mode.NO_RECHARGE) {

            for (CustomerRequest c : tmp) {
                c.getRequest().getFirst().setClosestStation(chargingStations);
                c.getRequest().getSecond().setClosestStation(chargingStations);
                s += Utilities.getDistance(c.getRequest().getFirst().getLocation(), c.getRequest().getFirst().getClosestStation());
                if (Utilities.getDistance(c.getRequest().getFirst().getLocation(), c.getRequest().getFirst().getClosestStation()) > maxDist) {
                    maxDist = Utilities.getDistance(c.getRequest().getFirst().getLocation(), c.getRequest().getFirst().getClosestStation());
                }
                if (Utilities.getDistance(c.getRequest().getFirst().getLocation(), c.getRequest().getFirst().getClosestStation()) < minDist) {
                    minDist = Utilities.getDistance(c.getRequest().getFirst().getLocation(), c.getRequest().getFirst().getClosestStation());
                }
            }

            if(prints)
                LOGGER.info("Average station dist: " + s / tmp.size() + ", Min station dist: " + minDist + ", Max station dist: " + maxDist + ", suggested (?) value: " + (maxDist - minDist) / 2.0 + ", other suggestion: " + ((maxDist - minDist) / 2.0 - s / tmp.size()));
        }

        if(prints)
            LOGGER.info("Unserviced customers: "+unserviced);

        this.unserviced=unserviced;

        //VRPPDVisualization visualization=new VRPPDVisualization(fleet,chargingStations,path,true);
        //visualization.visualize();

    }

    public double getBatteryCapacity(){
        return batteryCapacity;
    }

    public double getRechargeTime(){
        return rechargeTime;
    }

    public double getTotalLength(){
        return totalLength;
    }

    public double getTotalSlack(){
        return totalSlack;
    }

    public int getVehicles(){
        return vehicles;
    }

    public int getRecharges(){
        return recharges;
    }

    public int getUnserviced(){
        return unserviced;
    }

    public double getAverageBatteryLeft(){
        return averageBatteryLeft;
    }
}
