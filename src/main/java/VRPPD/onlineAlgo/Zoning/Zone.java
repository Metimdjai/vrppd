package VRPPD.onlineAlgo.Zoning;

import VRPPD.insertion.Customer;
import edu.uci.ics.jung.graph.util.Pair;

import java.util.LinkedList;

public class Zone {
    private Pair<Double> startingLocation;
    private Pair<Double> finalLocation;
    private Pair<Double> zoneTW;
    private LinkedList<Customer> customers;
    private LinkedList<Pair<Double>> margins;

    public Zone(Pair<Double> startingLocation,Pair<Double> finalLocation,Pair<Double> zoneTW){
        this.startingLocation=new Pair<Double>(startingLocation);
        this.finalLocation=new Pair<Double>(finalLocation);
        this.zoneTW=new Pair<Double>(zoneTW);
        customers=new LinkedList<Customer>();
        margins=new LinkedList<Pair<Double>>();
    }

    public void addCustomer(Customer customer){
        customers.addLast(customer);
        margins.add(new Pair<Double>(0.0,0.0));
        finalLocation=customer.getLocation();
    }

}
