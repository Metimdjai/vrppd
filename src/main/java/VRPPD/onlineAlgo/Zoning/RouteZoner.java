package VRPPD.onlineAlgo.Zoning;

import VRPPD.insertion.Customer;
import VRPPD.insertion.Route;

import java.util.LinkedList;

public class RouteZoner {
    private Route route;
    private LinkedList<Zone> zonedRoute;

    public RouteZoner(Route route){
        this.route=new Route(route);
        this.zonedRoute=new LinkedList<Zone>();
    }

    public void formZones(){
        for(Customer customer:route.getCustomers()){
            addLocInARouteServiceZone(customer);
        }
    }

    private void addLocInARouteServiceZone(Customer customer){

    }

    public LinkedList<Zone> getZonedRoute(){
        return zonedRoute;
    }
}
