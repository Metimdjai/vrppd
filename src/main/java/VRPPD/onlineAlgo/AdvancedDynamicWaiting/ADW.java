package VRPPD.onlineAlgo.AdvancedDynamicWaiting;

import VRPPD.insertion.Route;
import VRPPD.onlineAlgo.Zoning.Zone;

import java.util.LinkedList;

public class ADW {
    private LinkedList<Zone> zonedRoute;
    private Route scheduledRoute;

    public ADW(LinkedList<Zone> zonedRoute){
        this.zonedRoute=new LinkedList<Zone>(zonedRoute);
    }

    public Route performADWScheduling(){
        //TODO: separate zones offering space in between them proportional to the sum of the customers' slack times within each zone

        scheduledRoute=new Route(reconstructRoute());
        return scheduledRoute;
    }

    private Route reconstructRoute(){
        //TODO: is this function really necessary?

        return scheduledRoute;
    }


    //output

    @Override

    public String toString(){
        return (zonedRoute+","+scheduledRoute);
    }
}
