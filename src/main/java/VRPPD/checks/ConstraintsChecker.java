package VRPPD.checks;

import org.apache.log4j.Logger;

import VRPPD.insertion.Customer;
import VRPPD.insertion.Utilities;
import VRPPD.insertion.Route;
import VRPPD.onlineAlgo.OnlineVRPPD.Mode;

public class ConstraintsChecker {

	static Logger LOGGER = Logger.getLogger(ConstraintsChecker.class); 

    static boolean prints=false;

    //returns true iff route is feasible and satisfies all problem constraints

    public static boolean isFeasible(Route route,Mode mode,Double batteryCapacity){
        //a route is feasible iff:
        // -customer's margins don't overlap
        // -a delivery request of a customer always comes after its corrresponding pickup and the vehicle has enough capacity to serve customers
        // -if running algorithm with recharging strategies (electric vehicles) battery lasts for the full trip for each segment of the route
        if(mode!=Mode.NO_RECHARGE)
            return overlapsCheck(route) && pickupDeliveryConsistency(route) && capacityConstraint(route) && rechargingConstraint(route,batteryCapacity,mode);
        else
            return overlapsCheck(route) && pickupDeliveryConsistency(route) && capacityConstraint(route);
    }

    //returns true iff customers' margins do not overlap

    private static boolean overlapsCheck(Route route){
        for (int i = 1; i < route.getCustomers().size(); i++) {

            Customer c1 = route.getCustomers().get(i);
            Double curArrival, curWaiting;

            Double arrival = route.getMargins().get(i - 1).getFirst();
            Double waiting = route.getMargins().get(i - 1).getSecond();
            Double service = route.getCustomers().get(i - 1).getServiceTime();

            Double dist = Utilities.getDistance(c1.getLocation(), route.getCustomers().get(i - 1).getLocation());

            curArrival = arrival + waiting + service + dist;

            if (c1.getTimeWindow().getFirst() <= curArrival) {
                curWaiting = 0.0;
            } else {
                curWaiting = c1.getTimeWindow().getFirst() - curArrival;
            }

            if (curArrival + curWaiting > c1.getTimeWindow().getSecond()) {//if the customer cannot be serviced in time
                if(prints) {
                    LOGGER.info("overlaps check failed.");
                }
                return false;
            }

        }

        return true;
    }

    //returns true iff all delivery requests come after their corresponding pickup

    private static boolean pickupDeliveryConsistency(Route route){
        for(Customer c1:route.getCustomers()){//for every customer request
            boolean found=false;
            Customer c2=null;
            for(int i=route.getCustomers().indexOf(c1)+1;i<route.getCustomers().size();i++){//for every customer request AFTER c1
                c2=route.getCustomers().get(i);
                if(c1.getName().equals(c2.getName())){//if it is another request of the customer it will be either a pickup or a delivery one
                    found=true;
                    break;
                }
            }
            if(found){
                if(!(c1.isPickup()&&!c2.isPickup())){//if found and delivery request does not come after corresponding pickup for at least one client
                    if(prints) {
                        LOGGER.info("pickup delivery check failed");
                    }
                    return false;//the pickup and delivery are not consistent
                }
            }
            //if none exist(found=false), the client might have requested a pickup only or delivery only
        }
        return true;
    }

    //capacity constraint holds iff vehicle has infinite capacity or can serve its requests

    private static boolean capacityConstraint(Route route){
        return (route.getCapacity()==-1)||(route.getCapacity()>=0);
    }

    //recharghing constraint holds if vehicle has enough battery to travel from the last recharging station up to the closest possible at the end of its current segment

    private static boolean rechargingConstraint(Route route,Double batteryCapacity,Mode mode){

        //segment index points to last recharge station in the list which we must validate is reachable

        String[] stationName=route.getCustomers().get(route.getSegment()).getName().split(" ");

        return mode==Mode.RECHARGE?batteryCapacity>=Double.parseDouble(stationName[2]):Double.parseDouble(stationName[6])>=0;
    }
}
