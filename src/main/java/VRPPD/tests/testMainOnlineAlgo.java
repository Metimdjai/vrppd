package VRPPD.tests;

import VRPPD.onlineAlgo.OnlineVRPPD;
import VRPPD.onlineAlgo.OnlineVRPPD.Mode;

import com.google.common.io.Resources;

import java.io.File;
import java.util.Arrays;

public class testMainOnlineAlgo {

    public static void main(String[] args) {


        File path = new File(Resources.getResource("testCases").getPath());
        File[] files = path.listFiles();

        assert(files != null);
        Arrays.sort(files);

        int i=0;
        Double[][] length=new Double[4][4];
        Double[][] slack=new Double[4][4];
        double[][] vehicles=new double[4][4];
        double[][] recharges=new double[4][4];
        double[][] unserviced=new double[4][4];
        Double[][] averageBatteryLeft=new Double[4][4];

        int j=0;

        for(int k=0;k<4;k++){
            for(int l=0;l<4;l++){
                length[k][l]=slack[k][l]=averageBatteryLeft[k][l]=0.0;
                vehicles[k][l]=recharges[k][l]=unserviced[k][l]=0;
            }
        }

        for (File file : files) {
            if (file.isFile()) { //this line weeds out other directories/folders
                System.out.println(file);
                OnlineVRPPD onlineVRPPDNoRecharge = new OnlineVRPPD(file.getAbsolutePath(), Mode.NO_RECHARGE,0.0,0.0);
                OnlineVRPPD onlineVRPPDNaive = new OnlineVRPPD(file.getAbsolutePath(), Mode.RECHARGE,onlineVRPPDNoRecharge.getBatteryCapacity(),onlineVRPPDNoRecharge.getRechargeTime());
                OnlineVRPPD onlineVRPPDAdvancedRecharge = new OnlineVRPPD(file.getAbsolutePath(), Mode.ADVANCED_RECHARGE,onlineVRPPDNoRecharge.getBatteryCapacity(),onlineVRPPDNoRecharge.getRechargeTime());
                OnlineVRPPD onlineVRPPDUltraNaive = new OnlineVRPPD(file.getAbsolutePath(), Mode.ULTRA_NAIVE,onlineVRPPDNoRecharge.getBatteryCapacity(),onlineVRPPDNoRecharge.getRechargeTime());

                length[i][0]+=onlineVRPPDNoRecharge.getTotalLength();
                length[i][1]+=onlineVRPPDNaive.getTotalLength();
                length[i][2]+=onlineVRPPDAdvancedRecharge.getTotalLength();
                length[i][3]+=onlineVRPPDUltraNaive.getTotalLength();

                slack[i][0]+=onlineVRPPDNoRecharge.getTotalSlack();
                slack[i][1]+=onlineVRPPDNaive.getTotalSlack();
                slack[i][2]+=onlineVRPPDAdvancedRecharge.getTotalSlack();
                slack[i][3]+=onlineVRPPDUltraNaive.getTotalSlack();

                vehicles[i][0]+=onlineVRPPDNoRecharge.getVehicles();
                vehicles[i][1]+=onlineVRPPDNaive.getVehicles();
                vehicles[i][2]+=onlineVRPPDAdvancedRecharge.getVehicles();
                vehicles[i][3]+=onlineVRPPDUltraNaive.getVehicles();

                recharges[i][0]+=onlineVRPPDNoRecharge.getRecharges();
                recharges[i][1]+=onlineVRPPDNaive.getRecharges();
                recharges[i][2]+=onlineVRPPDAdvancedRecharge.getRecharges();
                recharges[i][3]+=onlineVRPPDUltraNaive.getRecharges();

                unserviced[i][0]+=onlineVRPPDNoRecharge.getUnserviced();
                unserviced[i][1]+=onlineVRPPDNaive.getUnserviced();
                unserviced[i][2]+=onlineVRPPDAdvancedRecharge.getUnserviced();
                unserviced[i][3]+=onlineVRPPDUltraNaive.getUnserviced();

                averageBatteryLeft[i][0]+=onlineVRPPDNoRecharge.getAverageBatteryLeft();
                averageBatteryLeft[i][1]+=onlineVRPPDNaive.getAverageBatteryLeft();
                averageBatteryLeft[i][2]+=onlineVRPPDAdvancedRecharge.getAverageBatteryLeft();
                averageBatteryLeft[i][3]+=onlineVRPPDUltraNaive.getAverageBatteryLeft();

                j++;
                if(j==10) {
                    i++;
                    j=0;
                }

                System.out.println(String.format("(%s) Total route length: "+onlineVRPPDNoRecharge.getTotalLength()+", Total slack time: "+onlineVRPPDNoRecharge.getTotalSlack()+", Number of vehicles: "+onlineVRPPDNoRecharge.getVehicles()+", Number of recharges: "+onlineVRPPDNoRecharge.getRecharges()+", Unserviced customers: "+onlineVRPPDNoRecharge.getUnserviced()+", Average Battery left: "+onlineVRPPDNoRecharge.getAverageBatteryLeft(),Mode.NO_RECHARGE));
                System.out.println(String.format("(%s) Total route length: "+onlineVRPPDNaive.getTotalLength()+", Total slack time: "+onlineVRPPDNaive.getTotalSlack()+", Number of vehicles: "+onlineVRPPDNaive.getVehicles()+", Number of recharges: "+onlineVRPPDNaive.getRecharges()+", Unserviced customers: "+onlineVRPPDNaive.getUnserviced()+", Average Battery left: "+onlineVRPPDNaive.getAverageBatteryLeft(),Mode.RECHARGE));
                System.out.println(String.format("(%s) Total route length: "+onlineVRPPDAdvancedRecharge.getTotalLength()+", Total slack time: "+onlineVRPPDAdvancedRecharge.getTotalSlack()+", Number of vehicles: "+onlineVRPPDAdvancedRecharge.getVehicles()+", Number of recharges: "+onlineVRPPDAdvancedRecharge.getRecharges()+", Unserviced customers: "+onlineVRPPDAdvancedRecharge.getUnserviced()+", Average Battery left: "+onlineVRPPDAdvancedRecharge.getAverageBatteryLeft(),Mode.ADVANCED_RECHARGE));
                System.out.println(String.format("(%s) Total route length: "+onlineVRPPDUltraNaive.getTotalLength()+", Total slack time: "+onlineVRPPDUltraNaive.getTotalSlack()+", Number of vehicles: "+onlineVRPPDUltraNaive.getVehicles()+", Number of recharges: "+onlineVRPPDUltraNaive.getRecharges()+", Unserviced customers: "+onlineVRPPDUltraNaive.getUnserviced()+", Average Battery left: "+onlineVRPPDUltraNaive.getAverageBatteryLeft(),Mode.ULTRA_NAIVE));
                System.out.println();
            }
        }

        System.out.println("Dataset groups comparison\n");

        for(i=0;i<4;i++){
            for(j=0;j<4;j++){
                length[i][j]/=10.0;
                slack[i][j]/=10.0;
                vehicles[i][j]/=10;
                recharges[i][j]/=10;
                unserviced[i][j]/=10;
                averageBatteryLeft[i][j]/=10.0;
            }
            System.out.println(String.format("(%s) Total Route length: "+length[i][0]+", Total slack time: "+slack[i][0]+", Number of vehicles: "+vehicles[i][0]+", Number of recharges: "+recharges[i][0]+", Unserviced customers: "+unserviced[i][0]+", Average Battery left: "+averageBatteryLeft[i][0],Mode.NO_RECHARGE));
            System.out.println(String.format("(%s) Total Route length: "+length[i][1]+", Total slack time: "+slack[i][1]+", Number of vehicles: "+vehicles[i][1]+", Number of recharges: "+recharges[i][1]+", Unserviced customers: "+unserviced[i][1]+", Average Battery left: "+averageBatteryLeft[i][1],Mode.RECHARGE));
            System.out.println(String.format("(%s) Total Route length: "+length[i][2]+", Total slack time: "+slack[i][2]+", Number of vehicles: "+vehicles[i][2]+", Number of recharges: "+recharges[i][2]+", Unserviced customers: "+unserviced[i][2]+", Average Battery left: "+averageBatteryLeft[i][2],Mode.ADVANCED_RECHARGE));
            System.out.println(String.format("(%s) Total Route length: "+length[i][3]+", Total slack time: "+slack[i][3]+", Number of vehicles: "+vehicles[i][3]+", Number of recharges: "+recharges[i][3]+", Unserviced customers: "+unserviced[i][3]+", Average Battery left: "+averageBatteryLeft[i][3],Mode.ULTRA_NAIVE));
            System.out.println();
        }
    }
}
