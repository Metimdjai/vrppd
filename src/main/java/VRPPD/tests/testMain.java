package VRPPD.tests;

import VRPPD.input.Reader;

import java.io.File;
import java.util.LinkedList;

import VRPPD.insertion.CustomerRequest;
import VRPPD.insertion.Route;
import com.google.common.io.Resources;
import edu.uci.ics.jung.graph.util.Pair;

public class testMain {

    public static void main(String[] args) {

        File dir = new File(Resources.getResource("testCases").getFile());
        File[] directoryListing = dir.listFiles();

        if (directoryListing != null) {
            for (File child : directoryListing) {

                System.out.println(child.getAbsolutePath().substring(0, child.getAbsolutePath().lastIndexOf('/')) + "/../Output_" + child.getName());

                Reader reader = new Reader(child.getAbsolutePath());//generate new vrppd instance

                LinkedList<CustomerRequest> customers=reader.getCustomers();
                System.out.println(customers);
                System.out.println();


                //new vehicle @(-1,3)
                Route r1=new Route(new Pair<Double>(-1.0,-3.0),0,0.0);

                //add first 4 customers from test case
                for(int i=0;i<4;i++){
                    r1.addCustomer(customers.get(i).getRequest().getFirst());
                    r1.addCustomer(customers.get(i).getRequest().getSecond());
                }

                System.out.println(r1.toString());
                System.out.println(r1.getMargins());

                System.out.println();

                //reposition customer 0+ to test margin recalculation
                r1.removeCustomerFrom(customers.get(0).getRequest().getFirst(),0);
                r1.addCustomer(customers.get(0).getRequest().getFirst());

                System.out.println(r1.toString());
                System.out.println(r1.getMargins());
            }
        } else {
            System.out.println("No directory found");
        }
    }
}
